# [MCUI.DESKTOP](http://webmcui.com)

MCUI是一个高定灵活、可组装可拆卸式的组件模块化的WEB前端框架, 由 [Junbo](https://junbo.name) , 并且托管在 [GitLab](https://gitlab.com/xiaojunbo).

开始使用, 请点击 <http://webmcui.com>!


## 目录

* [快速开始](#快速开始)
* [错误和功能请求](#错误和功能请求)
* [文档](#文档)
* [贡献](#贡献)
* [社区](#社区)
* [版本控制](#版本控制)
* [创作者](#创作者)
* [版权和许可](#版权和许可)


## 快速开始

有几种快速启动选项可供选择：

* [下载最新版本](https://webmcui.com/v1.0.0.zip).
* Git命令: `git clone https://gitlab.com/mcui/mcui.desktop.git`.
阅读 [入门页面](http://webmcui.com/getting-started/) 了解有关框架内容，模板和示例等的信息。

### 目录结构

在下载中，您将找到以下目录和文件，对常见资产进行逻辑分组，并提供编译和压缩版。你会看到这样的：

```
    
    说明：/ 目录文件夹    * 重要／核心    & 本文件位置
    
    mcui.desktop');
    ├── dev/                               开发环境目录
    │   ├── date]                          用于调试的数据目录
    │   ├── dist]                          编译打包后的文件目录( src -> dist )
    │   ├── docs]                          开发说明文档
    │   ├── src]                           开发源文件
    │   │   ├── fonts/                     字体目录，一般为iconfont
    │   │   ├── imgs/                      图标目录
    │   │   ├── js/                        js开发源文件
    │   │   │   ├── mcui.js                js开发源文件
    │   │   │   └── ux.js                  js开发源文件
    │   │   └── sass/                      scss开发源文件
    │   │       ├── custom/                放一次性定制样式
    │   │       ├── plugins/               放第三方插件样式
    │   │       ├── mcui/                * MCUI样式目录
    │   │       │   ├── base/              MCUI基础模块，全局性的模块，如重置，字体，色调，布局，网格等
    │   │       │   │   ├── mc-reset       
    │   │       │   │   ├── mc-base        
    │   │       │   │   ├── mc-size        
    │   │       │   │   ├── mc-color       
    │   │       │   │   └── ...            
    │   │       │   ├── module/            MCUI模块组件，UI组件，如按钮，输入框，导航，菜单等
    │   │       │   │   ├── mc-line        分融线 
    │   │       │   │   ├── mcui-btn         按钮
    │   │       │   │   ├── mc-input       输入框  
    │   │       │   │   ├── mc-textarea    文本域     
    │   │       │   │   ├── mc-rodia       单选  
    │   │       │   │   ├── mc-checkbox    复选     
    │   │       │   │   ├── mc-switch      开关   
    │   │       │   │   ├── mc-nav         导航
    │   │       │   │   ├── mc-menu        菜单 
    │   │       │   │   ├── mc-select      下拉选择   
    │   │       │   │   ├── mc-tabs        选项卡 
    │   │       │   │   ├── mc-table       表格  
    │   │       │   │   ├── mc-hint        提示 
    │   │       │   │   ├── mc-lable       标签  
    │   │       │   │   ├── mc-list        列表 
    │   │       │   │   ├── mc-card        卡片 
    │   │       │   │   ├── mc-toast       对话框  
    │   │       │   │   ├── mc-bar         条栏
    │   │       │   │   ├── mc-tobar       顶栏  
    │   │       │   │   ├── mc-fobar       底栏  
    │   │       │   │   ├── mc-badge       角标  
    │   │       │   │   ├── mc-play        轮播图 
    │   │       │   │   ├── mc-slide       侧滑  
    │   │       │   │   ├── mc-number      数字加减   
    │   │       │   │   ├── mc-grid        格子 
    │   │       │   │   ├── mc-title       标题  
    │   │       │   │   ├── mc-file        上传文件 
    │   │       │   │   ├── mc-paging      分页   
    │   │       │   │   └── ...            
    │   │       │   └── mcui.scss          MCUI主体文件，包含了MCUI所有模块
    │   │       └── theme/                 扩展的主题风格目录，一般情况下并不使用分离式主题，而是使用变量声明式切换主题，该目录仅为扩展保留
    │   │           └── default.scss       默认主题风格
    │   ├── views/                         静态页面／模板
    │
    │   └── index.html                     入口文件
    │
    ├── test/                              测试环境目录
    ├── pro/                               生产环境目录
    └── gulpfile.js                      & gulp任务文件
```


## 错误和功能请求

有错误或功能要求？请先阅读 [问题指南](https://webmcui.com/mcui.desktop/docs) 然后搜索现有和已解决的问题。如果您的问题或想法尚未解决，请打开一个[新问题](https://webmcui.com/mcui.desktop/issues/new).


## 文档

MCUI的文档，包括在根目录中的这个仓库，是由[Junbo](https://gitlab.com/xiaojunbo)构建的，并公开托管在GitLib页面https://gitlab.com/mcui/mcui.desktop。文档也可以在本地运行。


## 贡献
请阅读我们的贡献指南。包括打开问题，编码标准和开发注意事项的说明。
请阅读我们的 [贡献指南](https://webmcui.com/mcui.desktop/contributor). 包括打开问题，编码标准和开发注意事项的说明。


## 社区

获取有关MCUI开发的更新，并与项目维护者和社区成员聊天。

* MCUI官网[WEBMCUI](https://webmcui.com/mcui.desktop).
* 在微博上关注[@xiaojunbo](https://weibo.com/mcui).
* 加入QQ群 [374793742](374793742).


## 版本控制

为了使我们的发布周期更加透明，并努力保持向后兼容性，MCUI是根据语义版本标准来维护的。有时我们拧，但我们会尽可能遵守这些规则。


## 创作者

**Junbo**

* <https://junbo.name>


## 版权和许可
代码和文档版权所有2011-2016 Junbo.根据[MIT许可证](https://gitlab.com/mcui/mcui.desktop/blob/master/LICENSE.md)发布的代码。文档根据[知识共享(Creative Commons)](https://gitlab.com/mcui/mcui.desktop/blob/master/LICENSE.md)发布。



























